import os
import nnlog

server_url_info = {   #定义一个字典，将域名都放在字典里面
        'test':'http://yapi.idc.tencent.com',
        'dev' : 'http://yapi.idc.tencent.com'
}

ENV= 'test' #指定用哪个环境

server_url=server_url_info.get(ENV)

print(os.path.abspath(__file__))#当前文件的目录

path = os.path.dirname(os.path.abspath(__file__))#取当前文件目录的父目录
# print(path)
# os.path.abspath(__file__)

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) #取到atp-ql目录

log_path = os.path.join(BASE_PATH,'logs','atp-ql.log')#日志的路径
case_path = os.path.join(BASE_PATH,'case')#用例的路径
print(case_path)

logger =nnlog.Logger(log_path) #需要传入一个文件 #实例化  日志对象

